package com.xmas.pairser.domain.pairser.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Group {

    private Long id;
    private String name;
    private Set<Pair> pairs;
}
